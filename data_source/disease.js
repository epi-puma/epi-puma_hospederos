async function get_diseases(context) {
  const conn = await context.conn.psql;
  context.logger.log('Llamada a BD all_covariable');

  const query = `
    SELECT DISTINCT nombreenfermedad as name
    FROM ocurrence
    WHERE not nombreenfermedad is null
    `;

  console.log(query)

  const results = await conn.any(query);
  return results;
}

exports.get_diseases = get_diseases;