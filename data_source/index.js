const all_covariables = require('./covariable');
const ocurrence_by_id_covariable = require('./ocurrence');
const get_diseases = require('./disease');
const occurrences_by_taxon = require('./ocurrence');

exports.query_provider = {
  all_covariables: all_covariables,
  ocurrence_by_id_covariable: ocurrence_by_id_covariable,
  get_diseases: get_diseases,
  occurrences_by_taxon: occurrences_by_taxon,
};