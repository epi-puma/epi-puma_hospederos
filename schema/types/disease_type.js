const { gql } = require('apollo-server');

module.exports = gql`
  type Disease {
	name: String
  }
`;
