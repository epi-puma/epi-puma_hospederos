const { gql } = require('apollo-server');


const schema = gql`
  extend type Query {

    echo_hospederos(message: String): String,
    all_hospederos_covariables(limit: Int!, filter: String!): [CovariableHospederos!]!,
    ocurrence_hospederos_by_id_covariable(id_covariable: Int!, filter: String!): [OcurrenceHospederos!]!,
  	get_diseases_hospederos: [Disease]!
  	occurrences_by_taxon_hospederos(query: String!): [OcurrenceHospederos!]!

  }
`;

module.exports = schema;