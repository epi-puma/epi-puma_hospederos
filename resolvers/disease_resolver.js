const lodash = require('lodash');
const { query_provider } = require('../data_source');

const resolvers = {
	Query: {
	    get_diseases_hospederos: async (parent, {}, context) => {
	    	return query_provider.get_diseases.get_diseases(context);
		}
  	},
};

exports.resolvers = resolvers;
