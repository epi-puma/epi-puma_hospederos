const lodash = require('lodash');
const { query_provider } = require('../data_source');

const resolvers = {
	Query: {
	    ocurrence_hospederos_by_id_covariable: async (parent, {id_covariable, filter}, context) => {
			return query_provider.ocurrence_by_id_covariable.ocurrence_by_id_covariable(id_covariable, filter, context);	
		}
  	},
  	Query: {
  		occurrences_by_taxon_hospederos: async (parent, {query}, context) => {
			return query_provider.occurrences_by_taxon.occurrences_by_taxon(query, context);	
		}
  	}
};

exports.resolvers = resolvers;
